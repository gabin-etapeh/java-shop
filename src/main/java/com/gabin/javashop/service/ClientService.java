package com.gabin.javashop.service;

import com.gabin.javashop.model.entity.Client;
import com.gabin.javashop.repository.ClientRepository;
import com.gabin.javashop.repository.ProduitRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService {
    private final ClientRepository clientRepository;

    public ClientService (ClientRepository clientRepository){
        this.clientRepository = clientRepository;
    }

    public Client addClient (Client client){
        return clientRepository.save(client);
    }

    public Client getClient (Integer id){
        return clientRepository.findById(id).orElse(null);
    }

    public Optional<Client> updateClient (Integer id, Client nouveauClient){
        Optional<Client> ancienClientOptional = clientRepository.findById(id);
        if(ancienClientOptional.isPresent()){

            Client ancienClient = ancienClientOptional.get();
            ancienClient.setNom(nouveauClient.getNom());
            ancienClient.setPrenom(nouveauClient.getPrenom());
            ancienClient.setTelephone(nouveauClient.getTelephone());
            ancienClient.setAdresse(nouveauClient.getAdresse());
            ancienClient.setNumeroClient(nouveauClient.getNumeroClient());

            return Optional.of(clientRepository.save(ancienClient));
        } else {
            return Optional.empty();
        }
    }

    public void deleteClient(Integer id){
        clientRepository.deleteById(id);
    }
}
