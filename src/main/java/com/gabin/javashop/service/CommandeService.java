package com.gabin.javashop.service;

import com.gabin.javashop.model.dto.CommandeDto;
import com.gabin.javashop.model.dto.LigneCommandeDto;
import com.gabin.javashop.model.entity.Client;
import com.gabin.javashop.model.entity.Commande;
import com.gabin.javashop.model.entity.LigneCommande;
import com.gabin.javashop.model.entity.Produit;
import com.gabin.javashop.repository.ClientRepository;
import com.gabin.javashop.repository.CommandeRepository;
import com.gabin.javashop.repository.LigneCommandeRepository;
import com.gabin.javashop.repository.ProduitRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommandeService {

    private final CommandeRepository commandeRepository;
    private final ClientRepository clientRepository;
    private final ProduitRepository produitRepository;
    private final LigneCommandeRepository ligneCommandeRepository;

    public CommandeService (CommandeRepository commandeRepository,
                            ClientRepository clientRepository,
                            ProduitRepository produitRepository,
                            LigneCommandeRepository ligneCommandeRepository){
        this.commandeRepository = commandeRepository;
        this.clientRepository = clientRepository;
        this.produitRepository = produitRepository;
        this.ligneCommandeRepository = ligneCommandeRepository;
    }

    public CommandeDto addCommande (CommandeDto commandeDto){

        // 1. Retrouver le client associé à la commande
        Client client = retrouverClient(commandeDto.getIdClient());

        // 2. Créer une commande et lui attribuer le client retrouvé et l'enregistrer
        Commande commande = new Commande();
        commande.setDateCommande(commandeDto.getDateCommande());
        commande.setClient(client);
        Commande commandeEnregistree = commandeRepository.save(commande);

        // 3. Construire les lignes de commande en leur attribuant la commande nouvellement crée
        createLigneCommandes(commandeDto.getLignes(), commandeEnregistree);

        // 4. Retourner la commande crée sous forme de DTO
        return commandeEntityToDto(commandeRepository.save(commandeEnregistree));

    }

    private CommandeDto commandeEntityToDto(List<LigneCommande> ligneCommandes,
                                            Commande commande) {
        CommandeDto commandeDto= new CommandeDto();

       commandeDto.setIdCommande(commande.getNumeroCommande());
       commandeDto.setDateCommande(commande.getDateCommande());
       commandeDto.setIdClient(commande.getClient().getNumeroClient());

        List<LigneCommandeDto> ligneCommandesDto= new ArrayList<>();
        for (LigneCommande ligneCommande : ligneCommandes){
            LigneCommandeDto ligneCommandeDto = new LigneCommandeDto();
            ligneCommandeDto.setQuantite(ligneCommande.getQuantite());
            ligneCommandeDto.setProduit(getProduitById(ligneCommande.getIdProduit()));
            ligneCommandeDto.setCommande(commande);
            ligneCommandeDto.add(ligneCommandeDto);
            commandeDto.add(ligneCommandeDto);
        }
        ligneCommandeRepository.saveAll(ligneCommandes);

    return (commandeDto);
    }

    private void createLigneCommandes(List<LigneCommandeDto> lignes,
                                                     Commande commande) {

        List<LigneCommande> ligneCommandes = new ArrayList<>();

        for (LigneCommandeDto ligneCommandeDto : lignes){
            LigneCommande ligneCommande = new LigneCommande();
            ligneCommande.setQuantiteCommande(ligneCommandeDto.getQuantite());
            ligneCommande.setProduit(getProduitById(ligneCommandeDto.getIdProduit()));
            ligneCommande.setCommande(commande);
            ligneCommandes.add(ligneCommande);
        }
        ligneCommandeRepository.saveAll(ligneCommandes);
    }

    private Produit getProduitById(Integer idProduit) {
        return produitRepository.findById(idProduit).orElse(null);
    }

    private Client retrouverClient(Integer idClient) {
        return clientRepository.findById(idClient).orElse(null);
    }

    public CommandeDto getCommande (Integer id){
        return commandeEntityToDto(commandeRepository.findById(id).orElse(null));
    }

    public void deleteCommande(Integer id){
        commandeRepository.deleteById(id);
    }

}