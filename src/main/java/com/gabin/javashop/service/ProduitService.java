package com.gabin.javashop.service;

import com.gabin.javashop.model.entity.Produit;
import com.gabin.javashop.repository.ProduitRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class ProduitService {

    private final ProduitRepository produitRepository;

    public ProduitService (ProduitRepository produitRepository){
        this.produitRepository = produitRepository;
    }

    // Methode pour ajouter un produit
    public Produit addProduit (Produit produit){
        return produitRepository.save(produit);
    }

    // Methode pour récupérer un produit suivant son id
    public Produit getProduit (Integer id){
        return produitRepository.findById(id).orElse(null);
    }

    // Methode pour modifier un produit
    public Optional<Produit> updateProduit (Integer id, Produit nouveauProduit){
        Optional<Produit> ancienProduitOptional = produitRepository.findById(id);
        if(ancienProduitOptional.isPresent()){

            Produit ancienProduit = ancienProduitOptional.get();
            ancienProduit.setNomProduit(nouveauProduit.getNomProduit());
            ancienProduit.setPrix(nouveauProduit.getPrix());
            ancienProduit.setQuantite(nouveauProduit.getQuantite());

            return Optional.of(produitRepository.save(ancienProduit));
        } else {
            return Optional.empty();
        }
    }

    // Methode pour supprimer un produit
    public void deleteProduit(Integer id){
        produitRepository.deleteById(id);
    }
}
