package com.gabin.javashop.controller;

import com.gabin.javashop.model.entity.Client;
import com.gabin.javashop.model.entity.Produit;
import com.gabin.javashop.service.ClientService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
@RestController
public class ClientController {
    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;

    }
    @PostMapping("/client")
    public ResponseEntity<Client> addClient (@RequestBody Client client){
        return new ResponseEntity<>(clientService.addClient(client), HttpStatus.CREATED);
    }

    @GetMapping("/client/{id}")
    public ResponseEntity<Client> getClient (@PathVariable Integer id){
        return new ResponseEntity<>(clientService.getClient(id), HttpStatus.OK);
    }

    @PutMapping("/client/{id}")
    public ResponseEntity<?> updateClient(@PathVariable Integer id,
                                            @RequestBody Client nouveauClient){
        Optional<Client> updatedClient = clientService.updateClient(id, nouveauClient);
        if(updatedClient.isPresent()){
            return new ResponseEntity<>(updatedClient.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Aucun client à modifier", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/client/{id}")
    public ResponseEntity<Client> deleteClient (@PathVariable Integer id){
        clientService.deleteClient(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
