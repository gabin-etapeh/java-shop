
package com.gabin.javashop.controller;

import com.gabin.javashop.model.entity.Produit;
import com.gabin.javashop.service.ProduitService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class ProduitController {
    private final ProduitService produitService;

    public ProduitController(ProduitService produitService) {
        this.produitService = produitService;

    }
    @PostMapping("/produit")
    public ResponseEntity<Produit> addProduit (@RequestBody Produit produit){
        return new ResponseEntity<>(produitService.addProduit(produit), HttpStatus.CREATED);
    }

    // Une API pour récupérer un Produit suivant son identifiant [GET]
    @GetMapping("/produit/{id}")
    public ResponseEntity<Produit> getProduit (@PathVariable Integer id){
        return new ResponseEntity<>(produitService.getProduit(id), HttpStatus.OK);
    }

    // Une API pour modifier un Produit [PUT] ou [PATCH]
    @PutMapping("/produit/{id}")
    public ResponseEntity<?> updateProduit (@PathVariable Integer id,
                                            @RequestBody Produit nouveauProduit){
        Optional<Produit> updatedProduit = produitService.updateProduit(id, nouveauProduit);
        if(updatedProduit.isPresent()){
            return new ResponseEntity<>(updatedProduit.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Aucun produit à modifier", HttpStatus.NOT_FOUND);
        }
    }

    // Une API pour supprimer un Produit connaissant son identifiant [DELETE]
    @DeleteMapping("/produit/{id}")
    public ResponseEntity<Produit> deleteProduit (@PathVariable Integer id){
        produitService.deleteProduit(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
