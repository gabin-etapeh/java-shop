package com.gabin.javashop.controller;

import com.gabin.javashop.model.dto.CommandeDto;
import com.gabin.javashop.model.entity.Commande;
import com.gabin.javashop.service.CommandeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommandeController {

    private final CommandeService commandeService;

    public CommandeController(CommandeService commandeService) {
        this.commandeService = commandeService;

    }

    @PostMapping("/commande")
    public ResponseEntity<CommandeDto> addCommande (@RequestBody CommandeDto commandeDto){
        return new ResponseEntity<>(commandeService.addCommande(commandeDto), HttpStatus.CREATED);
    }

    @GetMapping("/commande/{id}")
    public ResponseEntity<CommandeDto> getCommande (@PathVariable Integer id){
        return new ResponseEntity<>(commandeService.getCommande(id), HttpStatus.OK);
    }

    @DeleteMapping("/commande/{id}")
    public ResponseEntity<Commande> deleteCommande (@PathVariable Integer id){
        commandeService.deleteCommande(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
