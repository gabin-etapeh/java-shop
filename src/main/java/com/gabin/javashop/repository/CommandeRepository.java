package com.gabin.javashop.repository;

import com.gabin.javashop.model.entity.Commande;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CommandeRepository extends JpaRepository<Commande, Integer> {

}
