package com.gabin.javashop.repository;

import com.gabin.javashop.model.entity.LigneFacture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LigneFactureRepository  extends JpaRepository<LigneFacture, Integer> {
}
