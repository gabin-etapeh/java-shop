package com.gabin.javashop.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommandeDto {

    private Integer idCommande;
    private Integer idClient;
    private LocalDate dateCommande;
    private List<LigneCommandeDto> lignes;

}
