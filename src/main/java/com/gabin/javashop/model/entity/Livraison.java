package com.gabin.javashop.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
public class Livraison {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer numeroLivraison;

  private LocalDate dateLivraison;

  @ManyToOne
  private Commande commande;
}
