package com.gabin.javashop.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
public class Produit{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer numeroProduit;

    private String nomProduit;
    private Integer quantite;
    private Double prix;

}

